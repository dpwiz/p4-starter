module Stage.Main.Types
  ( Stage
  , Frame

  , FrameResources(..)
  , RunState(..)
  ) where

import RIO

import Data.Tagged (Tagged)
import Geomancy (Transform, Vec2)
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.StageSwitch (StageSwitchVar)
import Engine.Worker (ObserverIO)
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Sun (Sun)
import Resource.Buffer qualified as Buffer

import Global.Render qualified as Rendering
import Stage.Main.UI (UI)
import Stage.Main.UI qualified as UI
import Stage.Main.World.Scene qualified as Scene

type Stage = Rendering.Stage FrameResources RunState

type Frame = Rendering.Frame FrameResources

data FrameResources = FrameResources
  { frSceneDescs :: Tagged '[Scene] (Vector Vk.DescriptorSet)
  , frSceneData  :: Buffer.Allocated 'Buffer.Coherent Scene
  , frScene      :: ObserverIO Scene

  , frSceneUiDescs :: Tagged '[Scene] (Vector Vk.DescriptorSet)
  , frSceneUiData  :: Buffer.Allocated 'Buffer.Coherent Scene
  , frSceneUi      :: ObserverIO Scene

  -- , rSunDescs :: Tagged '[Sun] (Vector Vk.DescriptorSet)
  -- , rSunData  :: Buffer.Allocated 'Buffer.Coherent Sun
  -- , rSunOut   :: ObserverIO (Storable.Vector Transform)

  -- , rEnv :: Env.Observer
  -- , rUI :: UI.Observer
  }

data RunState = RunState
  { rsNextStage   :: StageSwitchVar
  , rsProjectionP :: Camera.ProjectionProcess
  , rsViewP       :: Camera.ViewProcess

  , rsCursorPos :: Worker.Var Vec2
  , rsCursorP   :: Worker.Merge Vec2

  , rsSceneP :: Scene.Process
  , rsSceneV :: Scene.InputVar

  , rsSceneUiP :: Scene.Process

  -- , rsEnvP :: Env.Process
  -- , rsSunP :: Sun.Process
  -- , rsMoonP       :: Sun.Process
  -- , rsLightsP     :: Sun.LightsProcess

  -- , rsFonts    :: FontCollection
  -- , rsModels   :: GameModel.Collection
  -- , rsTextures :: CombinedCollection
  -- , rsCubeMaps :: CubeMapCollection

  , rsUI :: UI
  }
