module Stage.Main.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO

import Vulkan.Core10 qualified as Vk

import Engine.Types qualified as Engine
import Engine.Vulkan.DescSets (withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Swapchain qualified as Swapchain
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.ImGui qualified as ImGui

import Global.Render (ScreenPasses(..), Pipelines(..))
import Stage.Main.Types (FrameResources(..), RunState(..))
import Stage.Main.Render.UI (imguiDrawData)

updateBuffers
  :: RunState
  -> FrameResources
  -> Engine.StageFrameRIO ScreenPasses Pipelines FrameResources RunState ()
updateBuffers RunState{} FrameResources{} = do
  pure ()

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> Engine.StageFrameRIO ScreenPasses Pipelines FrameResources RunState ()
recordCommands cb FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask

  dear <- imguiDrawData

  ForwardMsaa.usePass (spForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    let dsl = Pipeline.pLayout $ pWireframe fPipelines
    withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS dsl frSceneDescs $
      pure ()

    ImGui.draw dear cb
