module Stage.Main.World.Scene where

import RIO

import Geomancy (Vec3, vec4)
import Geomancy.Vec4 qualified as Vec4
import Geomancy.Transform qualified as Transform

import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene(..))

type InputVar = Worker.Var Input

data Input = Input

initialInput :: Input
initialInput = Input

type Process = Worker.Merge Scene

mkScene :: Camera.Projection -> Camera.View -> Input -> Scene
mkScene Camera.Projection{..} Camera.View{..} Input = Scene{..}
  where
    sceneProjection    = projectionPerspective
    sceneInvProjection = Transform.inverse projectionPerspective -- FIXME: move to cell output

    sceneView          = viewTransform
    sceneInvView       = viewTransformInv
    sceneViewPos       = viewPosition
    sceneViewDir       = viewDirection

    sceneFog           = 0
    sceneEnvCube       = -1
    sceneNumLights     = 0
    sceneTweaks        = 0

mkSceneUi :: Camera.Projection -> Camera.View -> Input -> Scene
mkSceneUi camera@Camera.Projection{..} cameraView input =
  (mkScene camera cameraView input)
    { sceneProjection    = projectionOrthoUI
    , sceneInvProjection = Transform.inverse projectionOrthoUI -- FIXME: move to cell output
    , sceneView          = mempty
    , sceneInvView       = mempty
    }
