module Stage.Main.UI
  ( UI(..)
  , spawn
  ) where

import RIO

import Control.Monad.Trans.Resource qualified as Resource

import Engine.Camera qualified as Camera
import Engine.Types qualified as Engine
import Engine.Worker qualified as Worker

data UI = UI
  { display :: Worker.Var Bool
  }

type Assets = ()

spawn
  :: Assets
  -- -> Layout.BoxProcess
  -> Camera.ViewProcess
  -- -> Worker.Var sceneInput
  -- -> Env.Process
  -> Engine.StageRIO env (Resource.ReleaseKey, UI)
spawn _assets viewP = do
  display <- Worker.newVar True

  key <- Resource.register $ traverse_ Resource.release
    []

  pure (key, UI{..})
