module Stage.Main.Setup
  ( stackStage
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Geomancy (vec2, vec3, pattern WithVec2)
import RIO.App (appEnv)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.StageSwitch (getNextStage, newStageSwitchVar)
import Engine.Types (StackStage(..), StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (createSet0Ds_)
import Render.ImGui qualified as ImGui

import Global.Render qualified
import Stage.Main.Render qualified as Render
import Stage.Main.Types (FrameResources(..), RunState(..), Stage)
import Stage.Main.UI qualified as UI
import Stage.Main.World.Scene qualified as Scene

stackStage :: StackStage
stackStage = StackStage stage

stage :: Stage
stage = Engine.Stage
  { sTitle = "Main"

  , sAllocateP  = Global.Render.allocatePipelines True
  , sInitialRS  = initialRunState
  , sInitialRR  = initialFrameResources
  , sBeforeLoop = beforeLoop

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands
  , sGetNextStage   = getNextStage rsNextStage

  , sAfterLoop = afterLoop
  }
  where
    beforeLoop = do
      key <- Resource.register $ pure () -- TODO: Events.spawn
      ImGui.beforeLoop True
      pure key

    afterLoop key = do
      ImGui.afterLoop
      Resource.release key

initialRunState :: StageRIO env (Resource.ReleaseKey, RunState)
initialRunState = do
  rsNextStage <- newStageSwitchVar

  rsProjectionP <- asks $ Engine.ghScreenP . appEnv
  rsCursorPos <- Worker.newVar 0
  (cursorKey, rsCursorP) <- Worker.registered $
    Worker.spawnMerge2
      (\Camera.ProjectionInput{projectionScreen} (WithVec2 windowX windowY) ->
        let
          Vk.Extent2D{width, height} = projectionScreen
        in
          vec2
            (windowX - fromIntegral width / 2)
            (windowY - fromIntegral height / 2)
      )
      (Worker.getInput rsProjectionP)
      rsCursorPos

  let cameraTarget = vec3 0 0 0
  (viewKey, rsViewP) <- Worker.registered $
    Worker.spawnCell Camera.mkViewOrbital_ Camera.ViewOrbitalInput
      { orbitAzimuth  = τ/8
      , orbitAscent   = τ/12
      , orbitDistance = 0.5
      , orbitScale    = 1000
      , orbitTarget   = cameraTarget
      }

  -- (sunKey, rsSunP) <- Worker.registered $
  --   Worker.spawnCell mkSun SunInput
  --     { siInclination = τ/8
  --     , siAzimuth     = -τ/8

  --       -- TODO: get from direction
  --     , siColor = vec4 1 0.75 0.66 1

  --       -- TODO: get from view frustum and scene BB
  --     , siRadius     = 2 ** (0.6692913 * 16)
  --     , siDepthRange = 2 ** (0.7637795 * 16)
  --     , siSize       = 768
  --     , siTarget     = 0

  --     , siShadowIx   = 0
  --     }

  rsSceneV <- Worker.newVar Scene.initialInput
  (sceneKey, rsSceneP) <- Worker.registered $
    Worker.spawnMerge3 Scene.mkScene rsProjectionP rsViewP rsSceneV
  (sceneUiKey, rsSceneUiP) <- Worker.registered $
    Worker.spawnMerge3 Scene.mkSceneUi rsProjectionP rsViewP rsSceneV

  -- (envKey, rsEnvP) <- Worker.registered $
  --   Env.spawn Env.Input
  --     { azimuth     = 70/360
  --     , inclination = -0/360
  --     , altitude    = 0.5
  --     }

  -- (screenKey, rsScreenBoxP) <- Worker.registered Layout.trackScreen
  let assets = ()
  (uiKey, rsUI) <- UI.spawn assets rsViewP

  releaseKeys <- Resource.register $ traverse_ Resource.release
    [ cursorKey
    -- , debugKey
    , viewKey
    , sceneKey, sceneUiKey
    -- , sunKey
    -- , envKey
    -- , screenKey
    , uiKey
    ]

  pure (releaseKeys, RunState{..})

initialFrameResources
  :: Queues Vk.CommandPool
  -> Global.Render.ScreenPasses
  -> Global.Render.Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
initialFrameResources _pools _passes pipelines = do
  (frSceneDescs, frSceneData, frScene) <- createSet0Ds_
    (Global.Render.getSceneLayout pipelines)

  (frSceneUiDescs, frSceneUiData, frSceneUi) <- createSet0Ds_
    (Global.Render.getSceneLayout pipelines)

  pure FrameResources{..}
