module Stage.Main.Render.UI
  ( imguiDrawData
  ) where

import RIO.Local

import Control.Monad.Trans.Resource qualified as ResourceT
import DearImGui qualified
import Geomancy (vec4, withVec2, withVec4)
import Geomancy.Transform qualified as Transform
import GHC.Float (double2Float)
import RIO.State (gets)
import Vulkan.Core10 qualified as Vk

import Engine.Types (StageFrameRIO)
import Engine.Vulkan.DescSets (Bound, Compatible)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Worker qualified as Worker
import Render.Draw qualified as Draw
import Render.ImGui qualified as ImGui
import Render.DescSets.Set0 (Scene)
import Resource.Buffer qualified as Buffer

import Global.Render qualified as Render
import Stage.Main.Types (FrameResources(..), RunState(..))

import DearImGui qualified

type DrawM = StageFrameRIO Render.ScreenPasses Render.Pipelines FrameResources RunState

imguiDrawData :: DrawM DearImGui.DrawData
imguiDrawData = fmap snd $ ImGui.mkDrawData do
  DearImGui.showMetricsWindow
