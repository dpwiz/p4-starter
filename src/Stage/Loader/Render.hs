{-# LANGUAGE OverloadedLists #-}

module Stage.Loader.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO.Local

import RIO.State (gets)
import Vulkan.Core10 qualified as Vk

import Engine.Types qualified as Engine
import Engine.Vulkan.DescSets (withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardMsaa
import Resource.Buffer qualified as Buffer

import Global.Render (ScreenPasses(..), Pipelines(..))
import Stage.Loader.Types (FrameResources(..), RunState(..))
import Stage.Loader.UI qualified as UI

updateBuffers
  :: RunState
  -> FrameResources
  -> Engine.StageFrameRIO ScreenPasses Pipelines FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Worker.observeIO_ rsSceneUiP frSceneUi \_old new -> do
    _same <- Buffer.updateCoherent [new] frSceneUiData
    pure new

  UI.observe rsUI frUI

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> Engine.StageFrameRIO ScreenPasses Pipelines FrameResources RunState ()
recordCommands cb FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask

  ui <- mapRIO fst $ gets rsUI
  uiMessages <- traverse Worker.readObservedIO (UI.messages frUI)
  background <- Worker.readObservedIO (UI.background frUI)
  spinner <- Worker.readObservedIO (UI.spinner frUI)

  ForwardMsaa.usePass (spForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    let dsl = Pipeline.pLayout $ pWireframe fPipelines
    withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS dsl frSceneUiDescs do
      -- Render UI
      Pipeline.bind cb (pUnlitTexturedBlend fPipelines) do
        Draw.indexed cb (UI.quadUV ui) background
        Draw.indexed cb (UI.quadUV ui) spinner

      Pipeline.bind cb (pEvanwSdf fPipelines) $
        traverse_ (Draw.quads cb) uiMessages
