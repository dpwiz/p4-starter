module Stage.Loader.Scene
  ( Process
  , spawn
  ) where

import RIO

import Geomancy.Transform qualified as Transform

import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene(..), emptyScene)

type Process = Worker.Merge Scene

spawn
  :: ( MonadUnliftIO m
     , Worker.HasOutput projection
     , Worker.GetOutput projection ~ Camera.Projection
     )
  => projection
  -> m Process
spawn = Worker.spawnMerge1 mkScene

mkScene :: Camera.Projection -> Scene
mkScene Camera.Projection{..} =
  emptyScene
    { sceneProjection    = projectionOrthoUI
    , sceneInvProjection = Transform.inverse projectionOrthoUI -- FIXME: move to cell output
    }
