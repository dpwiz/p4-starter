{-# LANGUAGE OverloadedLists #-}

module Stage.Loader.Setup
  ( bootstrap
  , stackStage
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import RIO.App (appEnv)
import RIO.State (gets)
import RIO.Vector.Partial ((!))
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk
-- import Vulkan.Utils.Debug qualified as Debug

import Engine.Stage.Bootstrap.Setup qualified as Bootstrap
import Engine.StageSwitch (getNextStage, newStageSwitchVar, trySwitchStage)
import Engine.Types (StackStage(..), Stage(..), StageSetupRIO)
import Engine.Types qualified as Engine
import Engine.UI.Layout qualified as Layout
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Geometry.Flat qualified as Flat
import Render.DescSets.Set0 (createSet0Ds)
import Resource.Collection qualified as Collection
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.CommandBuffer (withPools)
import Resource.Font qualified as Font
import Resource.Model qualified as Model
import Resource.Sound.OpenAL qualified as OpenAL
import Resource.Texture qualified as Texture
import Resource.Texture.Ktx1 qualified as Ktx1

import Global.Render qualified
import Stage.Loader.Render qualified as Render
import Stage.Loader.Types (FrameResources(..), RunState(..))
import Stage.Loader.Scene qualified as Scene
import Stage.Loader.UI qualified as UI

bootstrap
  :: Text
  -> (Font.Config, Font.Config)
  -> (FilePath, FilePath)
  -> ((Text -> StageSetupRIO ()) -> StageSetupRIO loaded)
  -> (loaded -> StackStage)
  -> StackStage
bootstrap titleMessage (smallFont, largeFont) (bgPath, spinnerPath) loadAction nextStage =
  Bootstrap.stackStage stackStageBootstrap $
    withPools \pools -> do
      void $! OpenAL.allocateDevice

      logInfo "Bootstrapping loader fonts"

      let
        fontConfigs = [smallFont, largeFont] :: Vector Font.Config
      (fontKey, fonts) <- Font.allocateCollection pools fontConfigs

      let
        texturePaths = [bgPath, spinnerPath] :: Vector FilePath
      (textureKey, textures) <- Texture.allocateCollectionWith
        (Ktx1.createTexture pools)
        texturePaths

      let
        fontContainers = fmap Font.container fonts
        combinedTextures = Collection.enumerate CombinedTextures.Collection
          { textures = textures
          , fonts    = fmap Font.texture fonts
          }

      let
        uiSettings = UI.Settings
          { titleMessage = titleMessage
          , backgroundIx = 0
          , spinnerIx    = 1

          , combined = combinedTextures
          , fonts    = fontContainers

          , smallFont = \fs -> fs ! 0
          , largeFont = \fs -> fs ! 1
          }

      loaderKey <- Resource.register $
        traverse_ @[] Resource.release
          [ fontKey
          , textureKey
          ]
      pure Setup{..}

data Setup fonts textures loaded = Setup
  { loadAction :: (Text -> StageSetupRIO ()) -> StageSetupRIO loaded
  , nextStage  :: loaded -> StackStage
  , uiSettings :: UI.Settings textures fonts
  , loaderKey  :: Resource.ReleaseKey
  }

stackStageBootstrap
  :: (Traversable fonts, Traversable textures)
  => Setup fonts textures loaded -> StackStage
stackStageBootstrap Setup{..} = stackStage loadAction nextStage uiSettings

stackStage
  :: (Traversable fonts, Traversable textures)
  => ((Text -> StageSetupRIO ()) -> StageSetupRIO loaded)
  -> (loaded -> StackStage)
  -> UI.Settings textures fonts
  -> StackStage
stackStage loadAction nextStage uiSettings =
  StackStage $ loaderStage loadAction nextStage uiSettings

loaderStage
  :: (Traversable fonts, Traversable textures)
  => ((Text -> StageSetupRIO ()) -> StageSetupRIO loaded)
  -> (loaded -> StackStage)
  -> UI.Settings textures fonts
  -> Global.Render.Stage FrameResources RunState
loaderStage loadAction nextStage uiSettings = Stage
  { sTitle = "Loader"

  , sAllocateP  = Global.Render.allocatePipelines False -- TODO: move to class
  , sInitialRS  = initialRunState loadAction nextStage uiSettings
  , sInitialRR  = initialFrameResources uiSettings
  , sBeforeLoop = pure ()

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sGetNextStage = getNextStage rsNextStage
  , sAfterLoop    = pure
  }

initialRunState
  :: ((Text -> StageSetupRIO ()) -> StageSetupRIO loaded)
  -> (loaded -> StackStage)
  -> UI.Settings textures fonts
  -> StageSetupRIO (Resource.ReleaseKey, RunState)
initialRunState loadAction nextStage uiSettings =
  withPools \pools -> do
    rsProjectionP <- asks $ Engine.ghScreenP . appEnv

    (sceneUiKey, rsSceneUiP) <- Worker.registered $
      Scene.spawn rsProjectionP

    rsNextStage <- newStageSwitchVar

    context <- ask

    rsQuadUV <- Model.createStagedL context pools (Flat.toVertices Flat.texturedQuad) Nothing
    quadKey <- Resource.register $ Model.destroyIndexed context rsQuadUV

    (screenKey, screenBoxP) <- Worker.registered Layout.trackScreen

    (uiKey, rsUI) <- UI.spawn pools screenBoxP uiSettings

    let
      updateProgress text = do
        logInfo $ "Loader: " <> display text
        Worker.pushInput (UI.progressInput rsUI) \msg -> msg
          { Message.inputText = text
          }

    releaseKeys <- Resource.register $
      traverse_ @[] Resource.release
        [ sceneUiKey
        , quadKey
        , screenKey
        , uiKey
        ]

    switcher <- async do
      loader <- async $ loadAction updateProgress
      link loader
      -- threadDelay 1e6
      waitCatch loader >>= \case
        Left oopsie -> do
          logError "Loader failed"
          throwM oopsie
        Right loaded -> do
          logInfo "Loader signalled a stage change"

          updateProgress "Done!"
          void $! atomically . trySwitchStage rsNextStage . Engine.Replace $
            nextStage loaded

    -- XXX: propagate exceptions from loader threads
    link switcher

    pure (releaseKeys, RunState{..})

initialFrameResources
  :: (Traversable fonts, Traversable textures)
  => UI.Settings fonts textures
  -> Queues Vk.CommandPool
  -> Global.Render.ScreenPasses
  -> Global.Render.Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
initialFrameResources UI.Settings{combined} _pools _passes pipelines = do
  (frSceneUiDescs, frSceneUiData, frSceneUi) <- createSet0Ds
    (Global.Render.getSceneLayout pipelines)
    (fmap snd combined)
    Nothing
    Nothing
    mempty -- XXX: no shadows on loader
    Nothing

  frUI <- gets rsUI >>=
    UI.newObserver

  pure FrameResources{..}
