module Stage.Loader.Types
  ( Stage
  , Frame

  , FrameResources(..)
  , RunState(..)
  ) where

import RIO

import Data.Tagged (Tagged)
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.StageSwitch (StageSwitchVar)
import Engine.Worker (ObserverIO)
import Render.DescSets.Set0 (Scene)
import Resource.Buffer qualified as Buffer

import Global.Render qualified as Rendering
import Stage.Loader.Scene qualified as Scene
import Stage.Loader.UI (UI)
import Stage.Loader.UI qualified as UI

type Stage = Rendering.Stage FrameResources RunState

type Frame = Rendering.Frame FrameResources

data FrameResources = FrameResources
  { frSceneUiDescs :: Tagged '[Scene] (Vector Vk.DescriptorSet)
  , frSceneUiData  :: Buffer.Allocated 'Buffer.Coherent Scene
  , frSceneUi      :: ObserverIO Scene

  , frUI :: UI.Observer
  }

data RunState = RunState
  { rsNextStage   :: StageSwitchVar

  , rsProjectionP :: Camera.ProjectionProcess
  , rsSceneUiP    :: Scene.Process

  , rsUI :: UI
  }
