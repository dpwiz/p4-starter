module RIO.Local
  ( module RIO
  , module RIO.Local
  ) where

import RIO

τ :: Float
τ = 2 * pi
