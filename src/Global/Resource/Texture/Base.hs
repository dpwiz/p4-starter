{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.Texture.Base
  ( Collection(..)
  , TextureCollection
  , paths
  ) where

import RIO

import Data.Distributive (Distributive(..))
import Data.Distributive.Generic (genericCollect)
import Data.Functor.Rep (Co(..), Representable)
import GHC.Generics (Generic1)

import Resource.Static as Static
import Resource.Texture (Texture, Flat)

data Collection a = Collection
  { black         :: a
  , flat          :: a
  , ibl_brdf      :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic, Generic1)
  deriving Applicative via (Co Collection)
  deriving anyclass (Representable)

instance Distributive Collection where
  collect = genericCollect

type TextureCollection = Collection (Int32, Texture Flat)

Static.filePatterns Static.Files "resources/textures/base"

paths :: Collection FilePath
paths = Collection
  { black         = BLACK_KTX_ZST
  , flat          = FLAT_KTX_ZST
  , ibl_brdf      = IBL_BRDF_LUT_KTX_ZST
  }
