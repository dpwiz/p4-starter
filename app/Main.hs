module Main (main) where

import RIO

import Engine.App (engineMain)

import Stage.Loader.Setup qualified as Loader
import Stage.Main.Setup qualified as Main
import Global.Resource.Font qualified as Font
import Global.Resource.Texture.Base qualified as Base

main :: IO ()
main =
  engineMain $
    Loader.bootstrap
      "Starter Project"
      fonts
      (splash, spinner)
      loadAction
      (\() -> Main.stackStage)
  where
    Font.Collection{..} = Font.configs
    fonts = (small, large)

    Base.Collection{..} = Base.paths
    splash = black
    spinner = ibl_brdf

    loadAction update = do
      update "Waaaait a little"
      threadDelay 5e6
      update "Waaaait a little more"
      threadDelay 4e6
      update "Waaaait a little more."
      threadDelay 3e6
      update "Waaaait a little more.."
      threadDelay 2e6
      update "Waaaait a little more..."
      threadDelay 1e6
      update "Done?"
